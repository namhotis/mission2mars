Camera.SPEED = 1000;


//Camera.prototype.follow = function (sprite) {
//    this.following = sprite;
//    sprite.screenX = 0;
//    sprite.screenY = 0;
//};

Camera.prototype.update = function () {
    // assume followed sprite should be placed at the center of the screen
    // whenever possible
    this.following.screenX = this.width / 2;
    this.following.screenY = this.height / 2;

    // make the camera follow the sprite
    this.x = this.following.x - this.width / 2;
    this.y = this.following.y - this.height / 2;
    // clamp values
    this.x = Math.max(0, Math.min(this.x, this.maxX));
    this.y = Math.max(0, Math.min(this.y, this.maxY));

    // in map corners, the sprite cannot be placed in the center of the screen
    // and we have to change its screen coordinates

    // left and right sides
    if (this.following.x < this.width / 2 ||
        this.following.x > this.maxX + this.width / 2) {
        this.following.screenX = this.following.x - this.x;
    }
    // top and bottom sides
    if (this.following.y < this.height / 2 ||
        this.following.y > this.maxY + this.height / 2) {
        this.following.screenY = this.following.y - this.y;
    }
};

Camera.prototype.move = function (deltax, deltay, dirx, diry) {
    this.x += dirx * Camera.SPEED * deltax;
    this.y += diry * Camera.SPEED * deltay * (7 / 32);
    this.x = Math.max(0, Math.min(this.x, this.maxX));
    this.y = Math.max(0, Math.min(this.y, this.maxY));
};

Game.load = function () {
    return [
        Loader.loadImage('tiles', 'assets/tiles4.png'),
        Loader.loadImage('rover', 'assets/rovers.png')
    ];
};

Game._drawMap = function () {
    map.layers.forEach(function (layer, index) {
        this._drawLayer(index);
    }.bind(this));
};

Game._drawLayer = function (layer) {
    var context = this.layerCanvas[layer].getContext('2d');
    context.clearRect(0, 0, demo.width, demo.height);

    var startCol = Math.floor(this.camera.x / map.twidth);
    var endCol = Math.min((startCol + (this.camera.width / map.twidth)), map.cols);
    var startRow = Math.floor(this.camera.y / map.theight);
    var endRow = (startRow + (this.camera.height / map.theight)) * (32 / 9);
    var offsetX = -this.camera.x + startCol * map.twidth;
    var offsetY = -this.camera.y + startRow * map.theight;
    var diamondNot = 0;
    for (var r = startRow; r <= endRow; r++) {
        for (var c = startCol; c <= endCol - 1; c++) {
            if (isOdd(r) == 1) {
                diamondNot = map.twidth / 2;
            } else {
                diamondNot = 0;
            }
            var tile = map.getTile(layer, c, r);
            if (layer != 2 && tile !== 0) {

                var x = ((c - startCol) * map.twidth + offsetX) + diamondNot - map.twidth;
                //            var y = (((r - startRow) * map.theight + offsetY) - map.theight*1.5)*(10/32);
                var y = ((((r - startRow) * map.theight + offsetY)) * (10.35 / 32) - (34) * layer) - map.theight;

            } else if (layer == 2 && tile !== 0) {
                var y = ((((r - startRow) * map.theight + offsetY)) * (10 / 32)) - map.theight;
                var x = ((c - startCol) * map.twidth + offsetX) + diamondNot - map.twidth;
            }
            if (tile !== 0) { // 0 => empty tile
                context.drawImage(
                    this.tileAtlas, // image
                    (tile - 1) * map.twidth, // source x
                    0, // source y
                    map.twidth, // source width
                    map.theight, // source height
                    Math.round(x) * zoom, // target x
                    Math.round(y) * zoom, // target y
                    map.twidth * zoom, // target width
                    map.theight * zoom // target height
                );
            }
        }

    }

};


Game.render = function () {
    // re-draw map if there has been scroll
    if (this.hasScrolled) {
        this._drawMap();
    }

    for (i = 0; i < map.layers.length; i++) {
        this.ctx.drawImage(this.layerCanvas[i], 0, 0);
    }
};