///////////////////////////
////                   ////
////     FUNCTIONS     ////
////                   ////
///////////////////////////



///////////////////////////
////                   ////
////        MAP        ////
////                   ////
///////////////////////////


function getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

//Fonction permettant de générrer une map "random" avec quand même une récurrence plus ou moins élevée
function randomWithProbability() {
    var notRandomNumbers = [1, 2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4, 3, 5, 5, 5, 5, 3, 7, 8, 2, 1, 11, 12, 13];
    var idx = Math.floor(Math.random() * notRandomNumbers.length);
    return notRandomNumbers[idx];
}

function randomWithProbability2() {
    var notRandomNumbers = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 27, 28, 29, 30];
    var idx = Math.floor(Math.random() * notRandomNumbers.length);
    return notRandomNumbers[idx];
}

function retirerTile() {
    map.arrayBlack[20] = 0;
    Game._drawMap;
}

//Génère une map random, en attendant qu'ils aient créé un "vrai" générateur de map
function createArrayMap() {
    var arrayMap = [];
    for (var c = 1; c <= map.cols; c++) {
        for (var r = 1; r <= map.rows; r++) {
            arrayMap.push(randomWithProbability());
        }
    }
    return arrayMap;
}


function createArrayMap2() {
    var flag1X = getRandomIntInclusive(5, map.rows);
    var flag1Y = getRandomIntInclusive(5, map.cols);
    var flag2X = getRandomIntInclusive(5, map.rows);
    var flag2Y = getRandomIntInclusive(5, map.cols);
    console.log("X Flag 1 : " + flag1X + "\nY Flag 1 : " + flag1Y + "\nX Flag 2 : " + flag2X + "\nY Flag 2 : " + flag2Y);
    var arrayMap = [];
    for (var c = 1; c <= map.cols; c++) {
        for (var r = 1; r <= map.rows; r++) {
            if (((r == flag1X) && (c == flag1Y)) || ((r == flag2X) && (c == flag2Y))) {
                arrayMap.push(18);
            } else {
                arrayMap.push(randomWithProbability2());
            }
        }
    }
    return arrayMap;
}

function changerGlace() {
    var testGlace = 0;
    for (var c = 1; c <= map.cols; c++) {
        for (var r = 1; r <= map.rows; r++) {
            testGlace += 1;
            if (map.array1[testGlace] == 11) {
                map.array1[testGlace] = 5;
            }
        }
    }
    Game._drawMap()
    minimap();
}

function createArrayMapBlack() {
    var arrayMap = [];
    for (var c = 1; c <= map.cols; c++) {
        for (var r = 1; r <= map.rows; r++) {
            arrayMap.push(17);
        }
    }

    return arrayMap;
}

var fogOfWarDeleted = function findArrayBlack() {
    var tileN = ((test.y - 2) * map.cols + test.x);
    var tileE = (test.y * map.cols + test.x + 1);
    if (isOdd(test.y)) {
        var tileSE = ((test.y + 1) * map.cols + (test.x + 1));
        var tileSO = ((test.y + 1) * map.cols + test.x);
        var tileNE = ((test.y - 1) * map.cols + test.x + 1);
        var tileNO = ((test.y - 1) * map.cols + test.x);
    } else {
        var tileSE = ((test.y + 1) * map.cols + test.x);
        var tileSO = ((test.y + 1) * map.cols + test.x - 1);
        var tileNE = ((test.y - 1) * map.cols + test.x);
        var tileNO = ((test.y - 1) * map.cols + test.x - 1);
    }
    var tileS = ((test.y + 2) * map.cols + test.x);
    var tileO = (test.y * map.cols + test.x - 1);
    var tileCenter = (test.y * map.cols + test.x);
    var tileARetirer = [tileCenter, tileE, tileO, tileN, tileS, tileSE, tileSO, tileNE, tileNO];
    return tileARetirer;
}

function redrawArrayMap() {
    for (var i = 0; i < fogOfWarDeleted().length; i++) {
        map.arrayBlack[fogOfWarDeleted()[i]] = 0;
        if (tour != 0) {

            Game._drawMap();
        }
    }
}



///////////////////////////
////                   ////
////       ZOOM        ////
////                   ////
///////////////////////////


var zoom = 1;

function ZoomIn() {
    if (zoom <= 3) {
        addZoomBar();
        zoom += 1;
        for (var layer = 0; layer <= map.layers.length - 1; layer++) {
            Game._drawLayer(layer);
        }
        test.afficherRover();
    }
}

function ZoomOut() {
    if (zoom >= 2) {
        removeZoomBar();
        zoom -= 1;
        for (var layer = 0; layer <= map.layers.length - 1; layer++) {
            Game._drawLayer(layer);
        }
        test.afficherRover();
    }
}






///////////////////////////
////                   ////
////     CAM SPEED     ////
////                   ////
///////////////////////////

var speed = 1;

function speedUp() {
    Camera.SPEED += 100;
}

function speedDown() {
    Camera.SPEED -= 100;
}


function chooseEvent(event) {
    var x = event.which || event.keyCode;
    if (x == 97) {
        if (speed <= 10) {
            speed++;
            speedUp();
        }
    }
    if (x == 122) {
        if (speed >= 1) {
            speed--;
            speedDown();
        }
    }
    if (x == 101) {
        ZoomIn();
    }
    if (x == 114) {
        ZoomOut();
    }
    if (x == 56) {
        test.moveRover("N");
    }
    if (x == 50) {
        test.moveRover("S");
    }
    if (x == 52) {
        test.moveRover("O");
    }
    if (x == 54) {
        test.moveRover("E");
    }
    if (x == 55) {
        test.moveRover("NO");
    }
    if (x == 57) {
        test.moveRover("NE");
    }
    if (x == 51) {
        test.moveRover("SE");
    }
    if (x == 49) {
        test.moveRover("SO");
    }
}






///////////////////////////
////                   ////
////     UTILTAIRES    ////
////                   ////
///////////////////////////

function isOdd(num) {
    return num % 2;
}

function afficherTexte(message) {
    var panneauAnnonce = document.getElementById("annonce");
    panneauAnnonce.innerHTML = message;
    var test2 = setTimeout(function () {
        retirerTest();
    }, 2000);

}

function retirerTest() {
    var panneauAnnonce = document.getElementById("annonce");
    panneauAnnonce.innerHTML = "";
}



///////////////////////////
////                   ////
////      CAMERA       ////
////                   ////
///////////////////////////

function Camera(map, width, height) {
    this.x = 0;
    this.y = 0;
    this.width = width + map.twidth * 2;
    this.height = height + map.theight;
    this.maxX = (map.cols * map.twidth - width);
    this.maxY = map.rows * map.theight - height;
}


///////////////////////////
////                   ////
////        UI         ////
////                   ////
///////////////////////////

function addZoomBar() {
    var zoomBarColor = document.getElementsByClassName('zoomSection');
    var x = (zoom - 1);
    zoomBarColor[x].classList.add("activated");
}

function removeZoomBar() {
    var zoomBarColor = document.getElementsByClassName('zoomSection');
    var x = zoom - 2;
    zoomBarColor[x].classList.remove("activated");
}




///////////////////////////
////                   ////
////      HORLOGE      ////
////                   ////
///////////////////////////

var secondes = 0;
var minutes = 0;
var speedTimer = 1;

function minutePlus() {
    secondes++;
    if (secondes == 60) {
        secondes = 0;

        minutes++;
    }
    affichageTimer();
}

function affichageTimer() {
    if (secondes < 10) {
        secondes = ("0" + secondes);
    }
    if (minutes < 10) {
        var minutesAffichage = ("0" + minutes);
    }
    document.getElementById('secondes').innerHTML = secondes;
    document.getElementById('minutes').innerHTML = minutesAffichage;
}

var timerInterval;

var isPaused = new Boolean("true");
isPaused = true;

function pauseTimer() {
    clearInterval(timerInterval);
    isPaused = true;
}

function playTimer() {
    clearInterval(timerInterval);
    speedTimer = 1;
    timerInterval = setInterval(minutePlus, (1000 / speedTimer));
    isPaused = false;
}

function fastForward() {
    clearInterval(timerInterval);
    speedTimer = 2;
    timerInterval = setInterval(minutePlus, (1000 / speedTimer));
}

function REALLYfastForward() {
    clearInterval(timerInterval);
    speedTimer = 4;
    timerInterval = setInterval(minutePlus, (1000 / speedTimer));
}




///////////////////////////
////                   ////
////      MECANICS     ////
////                   ////
///////////////////////////

var tour = 0;

function pauseGame() {
    pauseTimer();
}

function playGame() {
    playTimer();
}

function turnBegin() {
    console.log("Début du tour");
    tour++;
    console.log(tour);
    document.getElementById('turn').innerHTML = tour;
}

function turnCourse() {
    console.log("Insérer ici une action");
}

function turnEnd() {
    console.log("Fin du tour");
}

function tourLancement() {
    turnBegin();
    turnCourse();
    turnEnd();
}

var roverEnergie = 6;

function refillEnergieBar() {
    var zoomBarColor = document.getElementsByClassName('energieBar');
    var x = (roverEnergie);
    while (roverEnergie < 6) {
        zoomBarColor[x].classList.add("activated");
        roverEnergie += 1;
        x += 1;
    }
    afficherTexte("Vous utilisez un puit de glace, vous regagnez toute votre énergie !!!")
}

function addEnergieBar() {
    var zoomBarColor = document.getElementsByClassName('energieBar');
    if (roverEnergie < 6) {
        var x = (roverEnergie);
        roverEnergie += 1;
        afficherTexte("Une barre d'énergie est gagnée")
        zoomBarColor[x].classList.add("activated");
    } else {
        afficherTexte("Votre barre d'énergie est déjà pleine !")
    }
}

function removeEnergieBar() {
    var zoomBarColor = document.getElementsByClassName('energieBar');
    var x = roverEnergie;
    if (roverEnergie > 0) {
        var x = (roverEnergie - 1);
        roverEnergie -= 1;
        afficherTexte("Une barre d'énergie est perdue")
        zoomBarColor[x].classList.remove("activated");
    } else {
        afficherTexte("Votre barre d'énergie est déjà vide, vous ne pouvez pas perdre d'avantage d'énergie");
    }
}

function scanZone() {
    afficherTexte("Vous scannez la zone, vous connaissez donc ainsi l'altitute des 9 blocs les plus proches");
}