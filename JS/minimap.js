function minimap() {
    var minimap = document.getElementById('minimap');
    if (!minimap) {
        alert("Impossible de récupérer le canvas");
        return;
    }

    var contextMinimap = minimap.getContext('2d');
    if (!contextMinimap) {
        alert("Impossible de récupérer le context du canvas");
        return;
    }
    minimap.height = 620;
    minimap.width = 515;

    var x = 10;
    var y = 10;
    var arrayMinimap = 0;
    var width = 0;
    var height = 0;

    var context = minimap.getContext('2d');

    var diamondNot = 0;
    for (var c = 1; c <= map.rows; c++) {
        for (var r = 1; r <= map.cols - 1; r++) {
            x = r + 7;
            y = c + 7;
            if (isOdd(c) == 1) {
                diamondNot = 1;
            } else {
                diamondNot = 0;
            }
            if (map.array1[arrayMinimap] >= 1 && map.array1[arrayMinimap] <= 4) {
                contextMinimap.fillStyle = "red";
            } else if (map.array1[arrayMinimap] >= 5 && map.array1[arrayMinimap] <= 8) {
                contextMinimap.fillStyle = "gray";
            } else if (map.array1[arrayMinimap] >= 9 && map.array1[arrayMinimap] <= 12) {
                contextMinimap.fillStyle = "lightblue";
            } else {
                contextMinimap.fillStyle = "black";
            }
            if (diamondNot == 1) {
                contextMinimap.fillRect((x + 1), y, 1, 1);
            } else {
                contextMinimap.fillRect(x, y, 1, 1);
            }
            arrayMinimap += 1;
        }

    }
}

function flagMinimap() {
    var flagMinimap = document.getElementById('minimapFLag');
    if (!flagMinimap) {
        alert("Impossible de récupérer le canvas");
        return;
    }

    var contextFlagMinimap = flagMinimap.getContext('2d');
    if (!contextFlagMinimap) {
        alert("Impossible de récupérer le context du canvas");
        return;
    }
    flagMinimap.height = 620;
    flagMinimap.width = 515;

    var x = 10;
    var y = 10;
    var arrayFlagMinimap = 0;
    var width = 0;
    var height = 0;

    var context = flagMinimap.getContext('2d');

    var diamondNot = 0;
    //    contextFlagMinimap.strokeRectt(Game.camera.x, Game.camera.y, 100, 50);
    for (var r = 1; r <= map.rows; r++) {
        for (var c = 1; c <= map.cols - 1; c++) {
            x = r + 7;
            y = c + 7;
            if (isOdd(c) == 1) {
                diamondNot = 1;
            } else {
                diamondNot = 0;
            }
            if (map.array2[arrayFlagMinimap] == 18) {
                contextFlagMinimap.fillStyle = "lightgreen";
                contextFlagMinimap.fillRect(y, x, 10, 10);
            }

            if (r == Rover.x && c == Rover.y) {
                contextFlagMinimap.fillStyle = "darkgray";
                contextFlagMinimap.fillRect(y, x, 10, 10);
            }
            arrayFlagMinimap += 1;
        }

    }
}

function hideMinimap() {
    document.getElementById('minimapFLag').style.visibility = "hidden";
    document.getElementById('minimap').style.visibility = "hidden";
}

function showMinimap() {
    document.getElementById('minimapFLag').style.visibility = "visible";
    document.getElementById('minimap').style.visibility = "visible";
}