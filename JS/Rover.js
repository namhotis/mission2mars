function Rover(name, mapTwidth, mapTheight) {
    this.x = 5,
        this.y = 5,
        this.name = name,
        this.realX = calculX,
        this.realY = (this.y + 1) * (mapTheight) * (10.35 / 32) - mapTheight - (mapTheight * 20.7 / 32) + 40 - mapTheight / 2,
        this.helloRover = helloRover,
        this.afficherRover = afficherRover,
        this.drawCanvas = drawCanvas,
        this.lauchRover = lauchRover,
        this.tileAtlas = Loader.getImage('tiles'),
        this.moveCameraRoverRight = moveCameraRoverRight,
        this.moveCameraRoverLeft = moveCameraRoverLeft,
        this.moveCameraRoverUp = moveCameraRoverUp,
        this.moveCameraRoverDown = moveCameraRoverDown,
        this.moveRover = moveRover,
        this.coordRover = coordRover
};

function lauchRover() {
    this.drawCanvas();
    this.realX();
    this.afficherRover(18);
    redrawArrayMap();
}

function coordRover() {
    console.log("X:" + this.x + "\nY:" + this.y);
}

function moveCameraRoverRight() {
    this.realX -= Camera.SPEED / 60;
    this.afficherRover(18);
}

function moveCameraRoverLeft() {
    this.realX += Camera.SPEED / 60;
    this.afficherRover(18);
}

function moveCameraRoverUp() {
    this.realY += Camera.SPEED / 57;
    this.afficherRover(18);
}

function moveCameraRoverDown() {
    this.realY -= Camera.SPEED / 57;
    this.afficherRover(18);
}


function calculX() {
    if (isOdd(this.y)) {
        this.realX = ((this.x) * map.twidth) - map.twidth / 2 + 12
    } else {
        this.realX = ((this.x) * map.twidth) - 0.5 * map.twidth - map.twidth / 2 + 12
    }
}

function helloRover() {
    afficherTexte("Salut, moi c'est " + this.name + " ! \nJe serais votre Rover pour cette partie.");
}

var sourceX = 0;
var direction = 0;
var etape = 1;

function afficherRover(asset) {
    var contextRoverCanvas = roverCanvas.getContext("2d");
    if (!contextRoverCanvas) {
        alert("Impossible de récupérer le context");
        return;
    }

    contextRoverCanvas.clearRect(0, 0, demo.width, demo.height);
    var diametreBalle = 20;
    var image = new Image();
    image.src = "assets/rovers.png";
    contextRoverCanvas.drawImage(
        image, // image
        sourceX, // source x
        0, // source y
        map.twidth, // source width
        map.theight, // source height
        Math.round(this.realX) * zoom, // target x
        Math.round(this.realY) * zoom, // target y
        map.twidth * zoom, // target width
        map.theight * zoom // target height
    );
}

function moveRover(direction) {
    var contextRoverCanvas = roverCanvas.getContext("2d");
    contextRoverCanvas.clearRect(0, 0, demo.width, demo.height);
    var image = new Image();
    var xLimit = (map.twidth);
    var yLimit = (map.theight * (10.35 / 32) * 2);
    var speed = 2;
    var testLauncher = 0;
    var testLauncherX = 0;
    etape = 0;
    var alternationEtape = 0;
    var testLauncherY = 0;
    if (direction == "N") {
        animate();
        this.y -= 2;

        function animate() {
            testLauncher += speed;
            direction = 3;
            sourceX = (((etape) * map.twidth) + (direction * 399));
            if (testLauncher < yLimit) {
                test.realY -= speed;
                alternationEtape += 1;
                if (alternationEtape >= 5) {
                    etape += 1;
                    alternationEtape = 0;
                }
                if (etape >= 3) {
                    etape = 0;
                }
                requestAnimationFrame(animate);

            } else {
                cancelAnimationFrame(animate);
            }
            test.afficherRover(23);
        }
    } else if (direction == "S") {
        animate();
        tourLancement();
        this.y += 2;

        function animate() {
            direction = 1;
            sourceX = (((etape) * map.twidth) + (direction * 399));
            testLauncher += speed;
            if (testLauncher < yLimit) {
                test.realY += speed;
                alternationEtape += 1;
                if (alternationEtape >= 5) {

                    etape += 1;
                    alternationEtape = 0;
                }
                if (etape >= 3) {
                    etape = 0;
                }
                requestAnimationFrame(animate);

            } else {
                cancelAnimationFrame(animate);
            }
            test.afficherRover(22);
        }
    } else if (direction == "E") {
        animate();
        tourLancement();
        this.x += 1;

        function animate() {
            testLauncher += speed;
            direction = 0;
            sourceX = (((etape) * map.twidth) + (direction * 399));
            console.log(sourceX);
            if (testLauncher < xLimit) {
                test.realX += speed;
                alternationEtape += 1;
                if (alternationEtape >= 5) {
                    etape += 1;
                    alternationEtape = 0;
                }
                if (etape >= 3) {
                    etape = 0;
                }
                requestAnimationFrame(animate);

            } else {
                cancelAnimationFrame(animate);
            }
            test.afficherRover(21);
        }
    } else if (direction == "O") {
        animate();
        tourLancement();
        this.x -= 1;

        function animate() {
            testLauncher += speed;
            direction = 2;
            sourceX = (((etape) * map.twidth) + (direction * 399));
            if (testLauncher < xLimit) {
                test.realX -= speed;
                alternationEtape += 1;
                if (alternationEtape >= 5) {

                    etape += 1;
                    alternationEtape = 0;
                }
                if (etape >= 3) {
                    etape = 0;
                }
                requestAnimationFrame(animate);

            } else {
                cancelAnimationFrame(animate);
            }
            test.afficherRover(19);
        }
    } else if (direction == "NE") {
        animate();
        tourLancement();
        if (isOdd(this.y)) {
            this.x += 1;
        }
        this.y -= 1;

        function animate() {
            testLauncher += speed;
            direction = 5;
            sourceX = (((etape) * map.twidth) + (direction * 399));
            if (testLauncher < xLimit / 2) {
                test.realX += speed;
                test.realY -= speed / 1.93;
                alternationEtape += 1;
                if (alternationEtape >= 5) {

                    etape += 1;
                    alternationEtape = 0;
                }
                if (etape >= 3) {
                    etape = 0;
                }
                requestAnimationFrame(animate);

            } else {
                cancelAnimationFrame(animate);
            }
            test.afficherRover(20);
        }
    } else if (direction == "SE") {
        animate();
        tourLancement();
        if (isOdd(this.y)) {
            this.x += 1;
        }
        this.y += 1;

        function animate() {
            testLauncher += speed;
            direction = 4;
            sourceX = (((etape) * map.twidth) + (direction * 399));
            if (testLauncher < xLimit / 2) {
                test.realX += speed;
                test.realY += speed / 1.93;
                alternationEtape += 1;
                if (alternationEtape >= 5) {

                    etape += 1;
                    alternationEtape = 0;
                }
                if (etape >= 3) {
                    etape = 0;
                }
                requestAnimationFrame(animate);

            } else {
                cancelAnimationFrame(animate);
            }
            test.afficherRover(18);
        }
    } else if (direction == "NO") {
        animate();
        if (isOdd(this.y) == false) {
            this.x -= 1;
        }
        this.y -= 1;
        tourLancement();

        function animate() {
            testLauncher += speed;
            direction = 7;
            sourceX = (((etape) * map.twidth) + (direction * 399));
            if (testLauncher < xLimit / 2) {
                test.realX -= speed;
                test.realY -= speed / 2;
                alternationEtape += 1;
                if (alternationEtape >= 5) {

                    etape += 1;
                    alternationEtape = 0;
                }
                if (etape >= 3) {
                    etape = 0;
                }
                requestAnimationFrame(animate);

            } else {
                cancelAnimationFrame(animate);
            }
            test.afficherRover(25);
        }
    } else if (direction == "SO") {
        animate();
        if (isOdd(this.y) == false) {
            this.x -= 1;
        }
        this.y += 1;
        tourLancement();

        function animate() {
            testLauncher += speed;
            direction = 6;
            sourceX = (((etape) * map.twidth) + (direction * 399));
            if (testLauncher < xLimit / 2) {
                test.realX -= speed;
                test.realY += speed / 2;
                alternationEtape += 1;
                if (alternationEtape >= 5) {

                    etape += 1;
                    alternationEtape = 0;
                }
                if (etape >= 3) {
                    etape = 0;
                }
                requestAnimationFrame(animate);

            } else {
                cancelAnimationFrame(animate);
            }
            test.afficherRover(24);
        }
    }
    redrawArrayMap();
    this.coordRover();
}

function drawCanvas() {
    var roverCanvas = document.getElementById("roverCanvas");
    if (!roverCanvas) {
        alert("Impossible de récupérer le canvas");
        return;
    }
    roverCanvas.height = window.innerHeight;
    roverCanvas.width = window.innerWidth;
}


var test = new Rover("Wall-E", map.twidth, map.theight);