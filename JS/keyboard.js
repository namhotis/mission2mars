Game.init = function () {
    Keyboard.listenForEvents(
        [Keyboard.LEFT, Keyboard.RIGHT, Keyboard.UP, Keyboard.DOWN]);
    this.tileAtlas = Loader.getImage('tiles');
    this.camera = new Camera(map, demo.width, demo.height);

    // create a canvas for each layer
    this.layerCanvas = map.layers.map(function () {
        var c = document.createElement('canvas');
        c.width = window.innerWidth;
        c.height = window.innerHeight;
        return c;
    });

    // initial draw of the map
    this._drawMap();
};

Game.update = function (deltax, deltay) {
    this.hasScrolled = false;
    // handle camera movement with arrow keys
    var dirx = 0;
    var diry = 0;
    if (Keyboard.isDown(Keyboard.LEFT)) {
        dirx = -1;
        test.moveCameraRoverLeft();
    }
    if (Keyboard.isDown(Keyboard.RIGHT)) {
        dirx = 1;
        test.moveCameraRoverRight();
    }
    if (Keyboard.isDown(Keyboard.UP)) {
        test.moveCameraRoverUp();
        diry = -1;
    }
    if (Keyboard.isDown(Keyboard.DOWN)) {
        test.moveCameraRoverDown();
        diry = 1;
    }

    if (dirx !== 0 || diry !== 0) {
        this.camera.move(deltax, deltay, dirx, diry);
        this.hasScrolled = true;
    }
};

var Keyboard = {};

Keyboard.LEFT = 37;
Keyboard.RIGHT = 39;
Keyboard.UP = 38;
Keyboard.DOWN = 40;

Keyboard._keys = {};

Keyboard.listenForEvents = function (keys) {
    window.addEventListener('keydown', this._onKeyDown.bind(this));
    window.addEventListener('keyup', this._onKeyUp.bind(this));

    keys.forEach(function (key) {
        this._keys[key] = false;
    }.bind(this));
}

Keyboard._onKeyDown = function (event) {
    var keyCode = event.keyCode;
    if (keyCode in this._keys) {
        event.preventDefault();
        this._keys[keyCode] = true;
    }
};

Keyboard._onKeyUp = function (event) {
    var keyCode = event.keyCode;
    if (keyCode in this._keys) {
        event.preventDefault();
        this._keys[keyCode] = false;
    }
};

Keyboard.isDown = function (keyCode) {
    if (!keyCode in this._keys) {
        throw new Error('Keycode ' + keyCode + ' is not being listened to');
    }
    return this._keys[keyCode];
};