var xhr = new XMLHttpRequest();
var url = "https://mars-map-generator.herokuapp.com/api/v1/maps";
xhr.open("POST", url, true);
xhr.setRequestHeader("Content-Type", "application/json");

function appelAPI() {
    if (xhr.readyState === 4 && xhr.status === 200) {
        var jsontest = xhr.responseText;
        return jsontest;
    }
};
var data = JSON.stringify({
    "height": 500,
    "width": 500
});
xhr.send(data);

var map = {
    cols: 500,
    rows: 500,
    twidth: 133,
    theight: 104,
    getTile: function (layer, col, row) {
        return this.layers[layer][row * map.cols + col];
    }
};
//Si on veut rajouter des nouveaux layers : 
map.array1 = createArrayMap();
map.array2 = createArrayMap2();
map.arrayBlack = createArrayMapBlack();
map.layers = [map.array1, map.array2, map.arrayBlack];


//
// Asset loader
//

var Loader = {
    images: {}
};

Loader.loadImage = function (key, src) {
    var img = new Image();

    var d = new Promise(function (resolve, reject) {
        img.onload = function () {
            this.images[key] = img;
            resolve(img);
        }.bind(this);

        img.onerror = function () {
            reject('Could not load image: ' + src);
        };
    }.bind(this));

    img.src = src;
    return d;
};

Loader.getImage = function (key) {
    return (key in this.images) ? this.images[key] : null;
};

//
// Game object
//

var Game = {};

Game.run = function (context) {
    this.ctx = context;
    this._previousElapsed = 0;

    var p = this.load();
    Promise.all(p).then(function (loaded) {
        this.init();
        window.requestAnimationFrame(this.tick);
    }.bind(this));
};

Game.tick = function (elapsed) {
    window.requestAnimationFrame(this.tick);

    // clear previous frame
    this.ctx.clearRect(0, 0, demo.width, demo.height);

    // compute delta time in seconds -- also cap it
    var deltax = (elapsed - this._previousElapsed) / 1000.0;
    deltax = Math.min(deltax, 0.25); // maximum delta of 250 ms
    var deltay = (elapsed - this._previousElapsed) / 1000.0 * (32 * 10);
    deltay = Math.min(deltay, 0.25); // maximum delta of 250 ms
    this._previousElapsed = elapsed;

    this.update(deltax, deltay);

    this.render();
}.bind(Game);


// override these methods to create the demo
Game.init = function () {};
Game.update = function (deltax, deltay) {};
Game.render = function () {};

//
// start up function
//

window.onload = function () {
    demo.width = window.innerWidth + map.twidth;
    demo.height = window.innerHeight + map.theight;
    var context = document.getElementById('demo').getContext('2d');
    Game.run(context);
    minimap();
    flagMinimap();
    hideMinimap();
    playGame();
    test.lauchRover();
    setTimeout(function () {
        test.afficherRover(18);
    }, 30);
    //C'est ici que l'on placera tout le code servant à nos dessins.
};